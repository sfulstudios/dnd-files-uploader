import os
import requests
import sys
import subprocess
from dotenv import load_dotenv
import re

load_dotenv()

api_key_openai = os.getenv('API_KEY_OPENAI')
final_path = 'mixed.md'

archivos_y_tamanos_locales = []
archivos_y_tamanos_remotos = []

def checkForUpdates(): # DISABLED
    return False # TODO Cambiar para auto-update
    version_info = {}

    commit_count = subprocess.run(["git", "rev-list", "--count", "HEAD"], capture_output=True, text=True).stdout.strip()
    version_info['commit_count'] = commit_count

    commit_hash = subprocess.run(["git", "rev-parse", "HEAD"], capture_output=True, text=True).stdout.strip()
    version_info['commit_hash'] = commit_hash

    print('\n')
    print('----- VERSION:', commit_count, '(' + commit_hash + ') -----')
    print('\n')
    print('----- CHECKING FOR UPDATES... -----')
    print('\n')

    # Verificar si hay cambios
    subprocess.run(["git", "fetch"], check=True)

    # Comprobar si hay diferencias
    result = subprocess.run(["git", "diff", "HEAD", "origin/main"], capture_output=True, text=True)

    # Si hay diferencias, actualiza el repositorio
    if result.stdout:
        print('\n')
        print("New version available. Updating...")
        print('\n')

        subprocess.run(["git", "pull"], check=True)

        commit_count = subprocess.run(["git", "rev-list", "--count", "HEAD"], capture_output=True, text=True).stdout.strip()
        version_info['commit_count'] = commit_count

        commit_hash = subprocess.run(["git", "rev-parse", "HEAD"], capture_output=True, text=True).stdout.strip()
        version_info['commit_hash'] = commit_hash
        print('\n')
        print("Updated to new version!")
        print('\n')
        return True
    else:
        print('\n')
        print("Up to date.")
        print('\n')
        return False

def listar_archivos_y_tamanos(carpeta):
    global archivos_y_tamanos_locales
    
    total_content = ""
    """ Devuelve una lista de tuplas con los archivos y sus tamaños en bytes en la carpeta dada, incluyendo subcarpetas. """
    print('\n')
    print('----- LOCAL FILES -----')
    for ruta_actual, carpetas, archivos in os.walk(carpeta):
        carpetas[:] = [carpeta for carpeta in carpetas if not carpeta.startswith('.')]
        for archivo in archivos:
            if archivo.startswith('.'):
                continue
            if not archivo.endswith('.md'):
                continue
            if archivo.endswith('.md'):
                ruta_completa = os.path.join(ruta_actual, archivo)
                with open(ruta_completa, 'r', encoding='utf-8') as archivo_md:
                    title_content = '{' + archivo[:-3].replace("_", " ").upper() + '} \n'
                    content = archivo_md.read()
                    content_wo_links = re.sub(r"\[.*?\]\(.*?\)", "", content)
                    content_wo_churros = re.sub(r"\[\[(.*?)\]\]", r"\1", content_wo_links)
                    content_wo_dm_notes = re.sub(r"\{\{NOTAS DEL DM\}\}.+?\{\{FIN NOTAS DEL DM\}\}", '', content_wo_churros, flags=re.DOTALL)
                    content_wo_hashtags = re.sub(r'\#\S+', '', content_wo_dm_notes)
                    total_content += title_content + content_wo_hashtags + "\n\n"
        with open(final_path, 'w', encoding='utf-8') as archivo_resultado:
            archivo_resultado.write(total_content)
        
    if os.path.isfile(final_path):
        tamano = os.path.getsize(final_path)
        archivos_y_tamanos_locales.append({'filename': final_path, 'bytes': tamano, 'filepath': final_path})
    # Imprimir los resultados
    print(f"File: {final_path}, Size: {tamano} bytes")       
    print('\n')
    print(len(archivos_y_tamanos_locales), 'files.') 
    return archivos_y_tamanos_locales

def getDataFromEndpoint(method, endpoint, headers, key, pathUploadFile, file_id):
    """ Hace una solicitud GET a un endpoint con encabezados personalizados y devuelve el valor de una clave específica del JSON de la respuesta. """
    if (method == 'get'): 
        respuesta = requests.get(endpoint, headers=headers)
    if (method == 'get100'): 
        respuesta = requests.get(endpoint, headers=headers, params={'limit': 100})
    if (method == 'post'):
        if (file_id):
            respuesta = requests.post(endpoint, headers=headers, json={'file_id': file_id})
        else:
            respuesta = requests.post(endpoint, headers=headers)
    if (method == 'postfile'):
        with open(pathUploadFile, 'rb') as uploadFile:
            newFiles = {'file': (pathUploadFile, uploadFile)}
            respuesta = requests.post(endpoint, headers=headers, data={'purpose': 'assistants'}, files=newFiles)
    if (method == 'delete'):
        respuesta = requests.delete(endpoint, headers=headers)

    datos_json = respuesta.json()  # Se llama sin argumentos adicionales
    
    if key:
        return datos_json.get(key)
    else:
        return datos_json



def getFilesIdFromAssistant():
    global archivos_y_tamanos_remotos
    endpoint = "https://api.openai.com/v1/assistants/asst_GZkZN1KZ9Pv3sSRi1WJSIL7M"
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }
    key = 'file_ids'

    ids = getDataFromEndpoint('get100', endpoint, headers, key, None, None)
    # TODO Añadir paginación (ahora solo devuelve 100)
    print('\n')
    print('----- ASSISTANT FILES -----')
    for id in ids:
        filename = getFilesNameById(id).get('filename')
        bytes = getFilesNameById(id).get('bytes')
        archivos_y_tamanos_remotos.append({"filename": filename, "bytes": bytes, "file_id": id})
        print(f"File: ", filename, ", Size: " + str(bytes) + " bytes")


def getFilesNameById(fileId):
    endpoint = "https://api.openai.com/v1/files/" + fileId
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }

    data = getDataFromEndpoint('get', endpoint, headers, None, None, None)
    return data

def deleteFileFromId(fileId):
    endpoint = "https://api.openai.com/v1/files/" + fileId
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }
    key = 'deleted'

    data = getDataFromEndpoint('delete', endpoint, headers, key, None, None)
    if (data == True):
        print('File deleted!')
        deleteFileFromAssistantById(fileId)
    else:
        print('\n')
        input('----- ERROR DELETING FILE! CALL TO CARLOS! -----')
        sys.exit()

def uploadFile(pathUploadFile):
    endpoint = "https://api.openai.com/v1/files"
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }
    key = 'status'

    data = getDataFromEndpoint('postfile', endpoint, headers, None, pathUploadFile, None)
    if (data.get('status') == 'processed'):
        print('File ' + data.get('id') + ' uploaded!')
        addFileToAssistantById(data.get('id'))
    else:
        print('\n')
        print(data)
        print('\n')
        input('----- ERROR UPLOADING FILE! CALL TO CARLOS! -----')
        sys.exit()
    return

def addFileToAssistantById(fileId):
    endpoint = "https://api.openai.com/v1/assistants/asst_GZkZN1KZ9Pv3sSRi1WJSIL7M/files"
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }

    data = getDataFromEndpoint('post', endpoint, headers, None, None, fileId)
    if (data):
        print('File ' + fileId + ' added to Assistant!')
        return

def deleteFileFromAssistantById(fileId):
    endpoint = "https://api.openai.com/v1/assistants/asst_GZkZN1KZ9Pv3sSRi1WJSIL7M/files/" + fileId
    headers = {
        "Authorization": 'Bearer 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',
        "OpenAI-Beta": "assistants=v1"
    }
    key = 'deleted'
    data = getDataFromEndpoint('delete', endpoint, headers, None, None, fileId)
    if (data.get('deleted') == True):
        print('File deleted from assistant!')
    else:
        print('\n')
        print(data)
        print('\n')
        input('----- ERROR DELETING FILE FROM ASSISTANT! CALL TO CARLOS! -----')
        sys.exit()
    return

def compareAllFiles():
    global archivos_y_tamanos_locales
    global archivos_y_tamanos_remotos

    if (len(archivos_y_tamanos_remotos) == 0):
        for archivo_local in archivos_y_tamanos_locales:
            print('\n')
            print('----- UPLOAD FILE -----')
            print('File ' + archivo_local['filepath'] + ' not found! Uploading file...')
            uploadFile(archivo_local['filepath'])
    else:
        for archivo_remoto in archivos_y_tamanos_remotos:
            found = False
            for archivo_local in archivos_y_tamanos_locales:
                if (archivo_local['filepath'] == archivo_remoto['filename']):
                    found = True
                    if (archivo_local['bytes'] != archivo_remoto['bytes']):
                        print('\n')
                        print('----- UPLOAD FILE -----')
                        print(archivo_local['filepath'] + ' has changed! Uploading changes...')
                        # TODO Borrar el archivo de asignados a Assistant. (En misma función de delete)
                        deleteFileFromId(archivo_remoto['file_id'])
                        uploadFile(archivo_local['filepath'])
        for archivo_local in archivos_y_tamanos_locales:
            found = False
            for archivo_remoto in archivos_y_tamanos_remotos:
                if (archivo_local['filepath'] == archivo_remoto['filename']):
                    found = True
            if (found == False):
                print('\n')
                print('----- UPLOAD FILE -----')
                print('File ' + archivo_local['filepath'] + ' not found! Uploading file...')
                uploadFile(archivo_local['filepath'])

            # TODO Falta posibilidad de que el archivo esté en remoto y no en local (BORRAR REMOTO).


def main():
    global archivos_y_tamanos_locales
    # Reemplaza 'tu_carpeta' con el camino de la carpeta que deseas explorar
    # carpeta = 'files'
    carpeta = input('Introduce el nombre de la carpeta donde se encuentran los archivos y pulsa intro: ')
    resultados = listar_archivos_y_tamanos(carpeta)

    
if __name__ == "__main__":
    if checkForUpdates():
        print('\n')
        print('Installing new version...')
        print('\n')
        subprocess.run(['python'] + sys.argv)
    else:
        main()
        getFilesIdFromAssistant()
        compareAllFiles()
        os.remove(final_path)
        print('\n')
        input("Script finalizado con éxito. Pulsa INTRO tecla para cerrar la ventana.")